



@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<nav class="navbar bg-light navbar-light navbar-expand-lg w-100">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-7">
               
            </div>



<div class="row">
    <div class="card mb-4">
        <div class="card-header" >
                <i class="fas fa-table me-1"></i>
               O'quvchilar
               <a style="float: right;" class=" btn btn btn-outline-success " href="/student/qoshish "><i class="bi bi-plus-circle"></i></a>
        </div>

        <div class="card-body">

<table id="datatablesSimple" class="table-dark table">

<thead>
    <tr>
        <th>#</th>
        <th>F.I.O</th>
        <th>Yutuqlari</th>
        <th>Rasm</th>
        <th>Action</th>
    </tr>
</thead>
<tfoot>
    <tr>
    <th>#</th>
        <th>F.I.O</th>
        <th>Yutuqlari</th>
        <th>Rasm</th>
        <th>Action</th>
    </tr>
</tfoot>
<tbody>
@foreach($students as $te)  
    <tr>
        <td>{{++$d}}</td>
        <td>{{$te->name}}</td>
        <td>{{$te->malumot}}</td>
      
        <td ><img height="100 px "  width="100 px" src="{{asset('/storage/student/'.$te->img)}}" alt=""></td>

        <td> <a href="/student/edit/{{$te->id}}" class="btn btn-outline-primary"><i
                    class="text-500 fas fa-edit"></i></a> 
                    
                    <a href="/student/delete/{{$te->id}}" class="btn btn-outline-primary"><i
                    class="text-500 fas fa-trash-alt"></i></a>
                    
                    <a href="{{asset('/storage/student/'.$te->img)}}" data-gall="porfolioGallery" class="venobox preview-link btn btn-outline-primary" ><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-image" viewBox="0 0 16 16">
  <path d="M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
  <path d="M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z"/>
</svg></a>


<button class="btn btn-outline-primary" data-toggle="modal" data-target="#ex{{$te->id}}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-card-text" viewBox="0 0 16 16">
  <path d="M14.5 3a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h13zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
  <path d="M3 5.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 8a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 8zm0 2.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5z"/>
</svg></button>
        <div class="modal fade" id="ex{{$te->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" ">
            <div class="modal-dialog modal-dialog-scrollable" style="color:black;">
                <div class="modal-content">
                    <div class="modal-header">
                        <p class="modal-title" id="staticBackdropLabel">{{$te->name}}</p>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{{$te->malumot}}</p></br>
                        <img src="{{asset('/storage/student/'.$te->img)}}" alt="" height="400 px" width="90%"  class="card-img">
                      </div>
                </div>
            </div>


</td>
    </tr>
    @endforeach
</tbody>
</table>

</div>
</div>
</div>
@endsection
