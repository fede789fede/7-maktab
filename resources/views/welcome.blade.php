@extends('layouts.user')

@section('content')
<section id="about" class="about">
  <div class="container" data-aos="fade-up">

    <div class="section-title">
      <h2 id="as1">Yangiliklar</h2>
    <section id="recent-blog-posts" class="recent-blog-posts">
        <div class="container" data-aos="fade-up">
          <div class="row"> 
          @foreach($news as $n)
          <div class="col-lg-4" style="margin-bottom: 15px;"> 
              <div class="post-box" id="">
                <div class="post-img" style="height:290px "><img src="{{asset('/storage/news/'.$n->img)}}" class="img-fluid"  alt="" >
               </div>
                <div class="portfolio-info" >
                
                </div>
                <span class="post-date"><i class="icofont-ui-calendar"></i>     {{$n->created_at}}</span>
                <h3 class="post-title" style="font-size:18px;">{{$n->titil}}</h3>
                <a href="/yangilik/{{$n->id}}" class="readmore stretched-link mt-auto" style="text-decoration:none;"><span>To'liq o'qish</span><i class="bi bi-arrow-right"></i></a>
              </div>
            </div>
            @endforeach
          </div> 
        
      </section> 

  </div>

@endsection

