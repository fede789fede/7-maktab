<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
// use Illuminate\Http\Download;
use Illuminate\Support\Facades\Storage;
use App\News;
use App\Rasm;
use App\Kitob;
use App\Rasm_tur;
use App\Teacher;
use App\Student;
use App\Raxbariyat;
use App\Sinf;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth',\App\Http\Middleware\AdminMiddleware::class]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
      
        $new=News::orderBy('id','desc')->get();
    
        return view('welcome',['news'=>$new,'d'=>1]);
    }
    public function gallerya(Request $request) {
        $rasm = Rasm::All();
        $tur = Rasm_tur::All();
        $new=News::orderBy('id','desc')->get();
        return view('gallerya',['rasm'=>$rasm,'tur'=>$tur,'news'=>$new,'d'=>1]);
        
    }
    public function yangilik(Request $request,$id) {
        $new=News::where('id',$id)->first();
        $news=News::orderBy('id','desc')->get();
        return view('yangilik',['news'=>$news,'new'=>$new,'d'=>1]);
        
    }
    public function manager(Request $request) {
        $new=Raxbariyat::All();
        $sinf = Sinf::orderBy('id','desc')->get();

        return view('raxbar',['raxbariyat'=>$new,'sinf'=>$sinf]);
        
    }
    public function manager_id(Request $request, $id) {
        $raxbars=Raxbariyat::All();
        $raxbar=Raxbariyat::where('id',$id)->first();

        return view('raxbar_id',['raxbar'=>$raxbar,'raxbars'=>$raxbars]);
        
    }
    public function teacher(Request $request) {
        $new=Teacher::All();
        $sinf = Sinf::orderBy('id','desc')->get();

        return view('teacher',['teachers'=>$new,'sinf'=>$sinf,'d'=>1]);
        
    }
    public function maktab(Request $request) {
        $news=News::orderBy('id','desc')->get();
        return view('maktab',['news'=>$news,'d'=>1]);
        
    }
    public function talabalar(Request $request) {
        $new=Student::All();
        $sinf = Sinf::orderBy('id','desc')->get();

        return view('student',['student'=>$new,'sinf'=>$sinf]);
        
    }
    public function kutubxona(Request $request) {
        $new=Kitob::orderBy('id','desc')->get();
        $sinf = Sinf::orderBy('id','desc')->get();
        $s=(string)\request()->get('sinf');
        if(!empty($s)){
            $new=  Kitob::orderBy('id','desc')->where('sinf',$s)->get();
        }
        
        $as="";
        if(empty($new[0])){
            $as="Hech  narsa topilmadi";
        }
      
        return view('kutubxona',['books'=>$new,'sinf'=>$sinf,'as'=>$as]);
        
    }
    public function download($id)
    {
        $book=Kitob::where('id',$id)->first();
        $joyi=$book->joyi;
        

        return Storage::disk('public')->download('/kitob/'.$joyi);
    }
}
