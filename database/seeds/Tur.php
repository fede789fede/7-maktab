<?php

use Illuminate\Database\Seeder;
use App\Rasm_tur;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class Tur extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tur=[
            'Maktab',
            'Tadbirlar',
            'Dars jarayoni'
        ];
        foreach($tur as $t){
            $tabla=new Rasm_tur();
            $tabla->name=$t;
            $tabla->save();
        }
    
    }
}
